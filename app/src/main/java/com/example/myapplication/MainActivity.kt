package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) =when (item.itemId) {
        R.id.Home->{
            startActivity(Intent(this, Home::class.java))
            true
        }
        R.id.Config->{
            startActivity(Intent(this, Config::class.java))
            true
        }
        R.id.Fav->{
            startActivity(Intent(this, Fav::class.java))
            true
        }
        R.id.Help->{
            startActivity(Intent(this, AyudaActivity::class.java))
            true
        }
        R.id.Data->{
            startActivity(Intent(this, Data::class.java))
            true
        }
        R.id.Dev->{
            startActivity(Intent(this, Dev::class.java))
            true
        }
        else -> super.onOptionsItemSelected(item)
    }
    fun msg(msg:String){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}